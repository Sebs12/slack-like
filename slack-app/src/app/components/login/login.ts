import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserLogin } from 'models';
import { AuthenticationService } from '../../services/index';
import { NzMessageService } from 'ng-zorro-antd';

/**
 * Connecte un utilisateur à la plateforme
 */
@Component({
    selector: 'login',
    templateUrl: 'login.html'
})
export class LoginComponent {
    model = new UserLogin();
    failed = false;
    constructor(
        private authService: AuthenticationService,
        private messageService: NzMessageService,
        private router: Router
    ) { }

    async login() {
        try {
            let res = await this.authService.authenticate(this.model)
            if(res)
                this.router.navigate(["/"])
        }
        catch (e) {
            const msg = "Username ou Mot de passe invalide"
            this.messageService.error(msg)
        }
    }
}
