import { Component, OnInit, Output, NgZone, Input } from '@angular/core';
import { NotificationService } from 'services';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router } from '@angular/router';


@Component({
    selector: 'notification-bar',
    templateUrl: 'notification-bar.html'
})
export class NotificationBarComponent implements OnInit {
    private notifications
    private elementClickable = ["Post", "Like", "Comment"]

    @Input() click

    constructor(
        private notificationService: NotificationService,
        private popin: NzNotificationService,
        private ngZone: NgZone,
        private router: Router
    ) {
    }

    ngOnInit() {
        window.addEventListener('load', function () {
            Notification.requestPermission(function (status) {
              console.log(status)
            })
        })


        this.notifications = JSON.parse(localStorage.getItem("notifications")) || []
        
        this.notificationService.subscribe((item, channelId, id) => {
            this.notifications.push({item, channelId, id})
            localStorage.setItem("notifications", JSON.stringify(this.notifications))

            let firstPart = item.user ? item.user.username : ''

            // Popin
            if(document.visibilityState === "hidden") {
              this.popin.blank(item.title, `${firstPart} ${item.message}`)
            }

            // Notification 
            this.notificationWeb(item.title, `${firstPart} ${item.message}`, channelId, id )

        })
    }

    onClick(notification) {
      this.elementClickable.forEach(async e => {
        if(e === notification.item.title) {
          this.scrollPage(notification.channelId, notification.id )  
        }
      })
    }

    notificationWeb(title: string, message: string, channelId: string,  id: string) {
        if(document.visibilityState === "hidden") {
            let n = new Notification("Slack-like", {body: message})
            
            n.onclick = e => {
              n.close()
              this.elementClickable.forEach(async e => {
                if(e === title) {
                  this.ngZone.run(() => {
                    this.scrollPage(channelId, id)
                  })
                }
              })
              
            }
          }
    }

    scrollPage(channelId, id) {
      const url = this.router.url.split("/")
  
      if(url[2].split("#")[0] !== channelId) {                    
        this.router.navigate(["/channel", channelId], {fragment: `${id}`})
      } else {
        document.getElementById(id).scrollIntoView();
      }
    }
}
