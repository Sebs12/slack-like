import { Component, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { Post } from 'models';
import { PostService, PostSocketService, LoggedUser, MessageParser } from 'services';


/**
 * Affiche les poste
 */
@Component({
  selector: 'post',
  templateUrl: 'post.html'
})
export class PostComponent implements AfterViewInit{
    ngAfterViewInit(): void {
        this.ready.emit(this.post.id);
    } 
    @Input() post: Post;
    @Output() ready: EventEmitter<string> = new EventEmitter();
    constructor(
        private postSocket: PostSocketService, 
        private user: LoggedUser,
        private postService: PostService,
        private parser: MessageParser
    ) {}

    ngOnInit() {
        this.post.content = this.parser.parse(this.post);
    }

    onComment(message: string) {
        if(!message) return
        this.postService.comment(this.post, message)
    }

    toggleLike() {
        if(!this.post.liked)
            this.postService.like(this.post).then(() => {
                this.post.liked = true
            })
    }
}
