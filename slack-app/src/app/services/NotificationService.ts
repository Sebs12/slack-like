import { Injectable } from '@angular/core';
import { Notification, User, Channel, Post, Like, Comment } from 'models';
import { LoggedUser } from './User';
import { SocketService } from './SocketService';
import { PostSocketService } from './PostSocketService';

@Injectable()
export class NotificationService{
    private postSocketService: PostSocketService;

    constructor(
        postSocketService: PostSocketService
    ) {
        this.postSocketService = postSocketService
    }

    subscribe(callback: (notification: Notification, channelId?: string,  id?: string) => void) { 
        this.postSocketService.onNewChannel(channel => {
            callback(this.createNotification("Channel", `Le channel ${channel.name} vient d'etre créé`))
        })

        this.postSocketService.onPost(post => {
            callback(this.createNotification("Post", "a crée un post", post.user), post.channel.id, post.id) 
        })

        this.postSocketService.onLike(like => {
            callback(this.createNotification("Like", "a aimé un post/commentaire", like.user), like.post.channel.id, like.post.id) 
        })

        this.postSocketService.onComment(comment => {
            callback(this.createNotification("Comment", "a commenté", comment.user), comment.post.channel.id, comment.post.id) 
        })

        this.postSocketService.onUserConnect(user => {
            callback(this.createNotification("User", "vient de se connecter")) 
        })
    }

    createNotification(title: string, message: string, user?) {
        const notification = new Notification()

        if(user) notification.user = user
        notification.title = title
        notification.message = message
        notification.creationTime = new Date().getTime()

        return notification
    }
}
